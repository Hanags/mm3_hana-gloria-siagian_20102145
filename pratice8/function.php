<?php
function writeMsg(){
    echo "Hello World!";
}
writeMsg();

echo "<br>";
echo "<br>";

function familyName($fname, $year){
    echo "$fname Refsnes. Born in $year <br>";
}
familyName("Hege", "1975");
familyName("Stale", "1978");
familyName("Kai Jim", "1983");

echo "<br>";
echo "<br>";

function sum(int $x,int $y){
    $z = $x + $y;
    return $z;
}

echo "5 + 10 = " . sum(5,10) . "<br>";
echo "7 + 13 = " . sum(7,13) . "<br>";
echo "2 + 4 = " . sum(2,4);

?>