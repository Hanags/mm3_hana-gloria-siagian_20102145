<?php
$x = 0;
while($x <= 100){
    echo "The number is: $x <br>";
    $x++;
}

echo "<br>";
echo "<br>";

$x1 = 6;

do{
    echo "The number is: $x1 <br>";
    $x1++;
}while($x1 <= 5);

echo "<br>";
echo "<br>";

for($x2 = 0; $x2 <= 10; $x2++){
    echo "The number is: $x2 <br>";
}

echo "<br>";
echo "<br>";

$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");

foreach($age as $x3 => $val){
    echo "$x3 = $val <br>";
}
?>